import subprocess, os, time, socket
from telegram.ext import Updater, MessageHandler, CommandHandler, Filters

USER_KEY = '789201325965142341'
MASTER_KEY = '1733057629882961909-388951707196906823811'
USER_AUTH = []

# 0 - Nothing
# 1 - Administrative
# 2 - User auth
# 3 - Everything
LOG_LEVEL = 3


def ip(bot, update):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    if LOG_LEVEL >= 3:
        log('Local IP got by ' + update.effective_user.full_name)
        update.message.reply_text('Local IP: ' + s.getsockname()[0])
    s.close()


def log(text):
    log_file = open('log.txt', mode='a', encoding='UTF-8')
    entry = time.ctime() + ': ' + text + '\n'
    log_file.write(entry)
    log_file.close()


def admin(bot, update, args):
    global LOG_LEVEL
    key = args[0]
    if key != MASTER_KEY:
        update.message.reply_text('Wrong master key')
        if LOG_LEVEL >= 1:
            log('Failed admin auth attempt from ' + update.effective_user.full_name)
            update.message.reply_text('Auth attempt logged')
        return 402
    else:
        update.message.reply_text('Master key accepted')
        if LOG_LEVEL >= 1:
            log('Admin auth accepted from ' + update.effective_user.full_name)
            update.message.reply_text('Auth logged')
    command = args[1]
    if command == 'revoke-all':
        global USER_AUTH
        USER_AUTH = []
        if LOG_LEVEL >= 1:
            log('Authentications reset by ' + update.effective_user.full_name)
            update.message.reply_text('Action logged')
    elif command == 'shutdown':
        if LOG_LEVEL >= 1:
            log('Bot shutdown by ' + update.effective_user.full_name)
            update.message.reply_text('Action logged')
        # noinspection PyProtectedMember
        os._exit(0)
    elif command == 'set-log-level':
        LOG_LEVEL = int(args[2])
        if LOG_LEVEL >= 1:
            log('Log level set to ' + str(args[2]) + ' by' + update.effective_user.full_name)
            update.message.reply_text('Action logged')


def auth(bot, update, args):
    key = args[0]
    if key == 'revoke':
        USER_AUTH.__delitem__(USER_AUTH.index(update.message.chat))
        update.message.reply_text('Access revoked')
        if LOG_LEVEL >= 2:
            log('Auth revoked from ' + update.effective_user.full_name)
            update.message.reply_text('Action logged')
    elif key == USER_KEY or key == MASTER_KEY:
        USER_AUTH.append(update.message.chat)
        update.message.reply_text('Auth key accepted')
        if LOG_LEVEL >= 2:
            log('Auth accepted from ' + update.effective_user.full_name)
            update.message.reply_text('Auth logged')
    else:
        update.message.reply_text('Wrong auth key')
        if LOG_LEVEL >= 2:
            log('Failed auth attempt from ' + update.effective_user.full_name)
            update.message.reply_text('Auth attempt logged')


def run(bot, update, args):
    if update.message.chat not in USER_AUTH:
        update.message.reply_text('Please authenticate to continue. "/auth <key>"')
        return 401
    command = ' '.join(args)
    if LOG_LEVEL >= 3:
        log('Command "' + command + '" executed by ' + update.effective_user.full_name)
    if command[:2] == 'cd':
        path = os.path.abspath(command[3:])
        os.chdir(path)
    try:
        out = subprocess.run(command, shell=True, stdout=subprocess.PIPE).stdout.decode('cp866')
        if out != '':
            update.message.reply_text('Command "' + str(command) + '" executed\n' + 'Response:\n' + out)
        else:
            update.message.reply_text('Command "' + str(command) + '" executed')
    except:
        update.message.reply_text('Command "' + str(command) + '" execution failed')


def download_file(bot, update, args):
    if update.message.chat not in USER_AUTH:
        update.message.reply_text('Please authenticate to continue. "/auth <key>"')
        return 401
    name = ' '.join(args)
    bot.send_document(chat_id=update.message.chat.id, document=open(name, 'rb'))


def upload_file(bot, update):
    if update.message.chat not in USER_AUTH:
        update.message.reply_text('Please authenticate to continue. "/auth <key>"')
        return 401
    fid = update.message.document.file_id
    print(fid)
    file = bot.get_file(fid)
    print(update.message.document.file_name, update.message.document.mime_type)
    file.download(update.message.document.file_name)
    print(file.file_path)


def echo(bot, update):
    if update.message.chat not in USER_AUTH:
        update.message.reply_text('Please authenticate to continue. "/auth <key>"')
        return 401
    if LOG_LEVEL >= 3:
        log('Command "' + str(update.message.text) + '" executed by ' + update.effective_user.full_name)
    if update.message.text[:2] == 'cd':
        path = os.path.abspath(update.message.text[3:])
        os.chdir(path)
    try:
        out = subprocess.run(update.message.text, shell=True, stdout=subprocess.PIPE).stdout.decode('cp866')
        if out != '':
            update.message.reply_text('Command "' + str(update.message.text) + '" executed\n' + 'Response:\n' + out)
        else:
            update.message.reply_text('Command "' + str(update.message.text) + '" executed')
    except:
        update.message.reply_text('Command "' + str(update.message.text) + '" execution failed')


def main():
    updater = Updater("584421164:AAF4sXxl8GW7WmRJMUUGlSGWVfjdkbzerJc")

    dp = updater.dispatcher

    dp.add_handler(MessageHandler(Filters.text, echo))
    dp.add_handler(MessageHandler(Filters.document, upload_file))
    dp.add_handler(CommandHandler("run", run, pass_args=True))
    dp.add_handler(CommandHandler("download", download_file, pass_args=True))
    dp.add_handler(CommandHandler("auth", auth, pass_args=True))
    dp.add_handler(CommandHandler("admin", admin, pass_args=True))
    dp.add_handler(CommandHandler("ip", ip))

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()
